package test.java.API;

import java.util.Map;
//import junit
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Order;
//import rest
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

//import utilities
import resources.java.utilities.*;

public class SessionApiTest {
	static Map<String, String> cookie;
	static String url = "http://automationpractice.com/index.php?";
	HttpHelper helper = new HttpHelper();
	
	@Test
	@Order(1)
	public void GIVEN_a_test_site_WHEN_registration_request_is_made_THEN_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "controller=authentication&back=my-account";
		String body = helper.ReturnRegistrationBody();
		String Url = url + "controller=authentication";
		
		//ACT
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginPostHeaders(referrer)).
		and().body(body).request(Method.POST, Url);
		
		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(2)
	public void GIVEN_a_test_site_WHEN_the_a_log_in_request_is_made_THEN_the_response_code_is_302()
	{
		//ARRANGE;
		String referrer = url + "controller=authentication&back=my-account";
		String body = "email=test%40test1110466429.com&passwd=tester&back=my-account&SubmitLogin=";
		String Url = url + "controller=authentication";
		
		//ACT
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginPostHeaders(referrer)).
		and().body(body).request(Method.POST, Url);
		
		//ASSERT
		assertEquals(302, response.getStatusCode());
	}
	
	@Test
	@Order(3)
	public void GIVEN_a_test_site_WHEN_the_user_adds_items_to_the_cart_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "id_product=3&controller=product&content_only=1";
		String body = "controller=cart&add=1&ajax=true&qty=1&id_product=3&token=6295937ae1811c2282f556d51e4c61e1&ipa=13";
		String Url = url + "rand=1629040554132";

		//ACT
		//Step 1: Add items to the cart
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginPostHeaders(referrer)).
		and().body(body).request(Method.POST, Url);
		
		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(4)
	public void GIVEN_a_test_site_WHEN_the_user_starts_the_order_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "id_category=8&controller=category";
		String Url = url + "controller=order";

		//ACT
		//Step 2: Start order
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginGetHeaders(referrer)).
		and().request(Method.GET, Url);

		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(5)
	public void GIVEN_a_test_site_WHEN_the_user_proceeds_to_checkout_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "controller=order";
		String Url = url + "controller=order&step=1";

		//ACT
		//Step 3: Proceed to checkout
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginGetHeaders(referrer)).
		and().request(Method.GET, Url);

		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(6)
	public void GIVEN_a_test_site_WHEN_the_user_confirms_the_order_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "controller=order&step=1";
		String body = "id_address_delivery=555064&same=1&message=&step=2&back=&processAddress==1";
		String Url = url + "controller=order";

		//ACT
		//Step 4: Confirm order
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginPostHeaders(referrer)).
		and().body(body).request(Method.POST, Url);
		
		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(7)
	public void GIVEN_a_test_site_WHEN_the_user_confirms_shipping_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "controller=order";
		String body = "delivery_option%5B555064%5D=2%2C&cgv=1&step=3&back=&processCarrier=";
		String Url = url + "controller=order&multi-shipping=";

		//ACT
		//Step 5: Confirm shipping
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginPostHeaders(referrer)).
		and().body(body).request(Method.POST, Url);
		
		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(8)
	public void GIVEN_a_test_site_WHEN_the_user_selects_a_banking_option_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "controller=order&multi-shipping=";
		String Url = url + "fc=module&module=bankwire&controller=payment";

		//ACT
		//Step 6: Select banking option
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginGetHeaders(referrer)).
		and().request(Method.GET, Url);

		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
	
	@Test
	@Order(9)
	public void GIVEN_a_test_site_WHEN_the_validates_their_banking_option_THEN_the_response_code_is_302()
	{
		//ARRANGE
		String referrer = url + "fc=module&module=bankwire&controller=payment";
		String body = "currency_payement=1";
		String Url = url + "fc=module&module=bankwire&controller=validation";

		//ACT
		//Step 7: Validate banking option
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginPostHeaders(referrer)).
		and().body(body).request(Method.POST, Url);
		
		//ASSERT
		assertEquals(302, response.getStatusCode());
	}
	
	@Test
	@Order(10)
	public void GIVEN_a_test_site_WHEN_the_user_confirms_their_order_THEN_the_response_code_is_200()
	{
		//ARRANGE
		String referrer = url + "fc=module&module=bankwire&controller=payment";
		String Url = url + "controller=order-confirmation&id_cart=3559946&id_module=3&id_order=351878&key=9a4f1f007d27b23544d6c3327de45f08";

		//ACT
		//Step 8: Confirm order
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.given().headers(helper.ReturnLoginGetHeaders(referrer)).
		and().request(Method.GET, Url);

		//ASSERT
		assertEquals(200, response.getStatusCode());
	}
}
