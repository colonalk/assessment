package test.java.UI;

import resources.java.pages.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
//import selenium
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;
//import junit
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
//import hamcrest
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


public class WebUiTest {
	
	static String url = "http://automationpractice.com/";
	static String registeredEmail = "test@test1110466429.com";
	static WebDriver driver;
	
	@BeforeEach
	public void Setup() {
		//setting the driver executable
		WebDriverManager.chromedriver().setup();

		//Set chrome to headless
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

		//Initiating your chromedriver
		driver = new ChromeDriver(options);

		//Applied wait time
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test
	@Order(1)
	public void GIVEN_a_test_site_WHEN_the_user_registers_THEN_registration_is_successful()	{
		//ARRANGE
		//Initialize pages
		LandingPage landingPage = new LandingPage(driver);
		LoginPage loginPage = new LoginPage(driver);
		RegistrationPage regPage = new RegistrationPage(driver);
		//open browser with desired URL
		driver.navigate().to(url);

		//ACT
		//Access the registration page
		landingPage.clickLogin();
		registeredEmail = loginPage.InputRegisterEmail();
		loginPage.clickCreateAnAccount();
		//Input registration data and click register
		regPage.InputUserInformation();
		regPage.ClickRegisterButton();
		
		//ASSERT
		assertEquals("test tester", landingPage.ReturnUserInfo());
	}
	
	@Test
	@Order(2)
	public void GIVEN_a_test_site_WHEN_the_user_checks_out_THEN_checkout_is_successful() {
		//ARRANGE
		//Initialize pages
		LandingPage landingPage = new LandingPage(driver);
		LoginPage loginPage = new LoginPage(driver);
		CheckoutPage checkoutPage = new CheckoutPage(driver);
		//open browser with desired URL
		driver.get(url);
		
		//ACT
		//Add Item to cart from landing page, and proceed to checkout
		landingPage.ClickProduct(1);
		landingPage.AddToCart(1);
		landingPage.ClickProceedToCheckout();
		//Proceed with checkout
		checkoutPage.ClickProceedToCheckout();
		//Login
		loginPage.InputEmail(registeredEmail);
		loginPage.InputPassword("tester");
		loginPage.clickSignIn();
		//Proceed with checkout
		checkoutPage.ClickProceedToCheckout();
		checkoutPage.AcceptTermsOfService();
		checkoutPage.ClickProceedToCheckout();
		checkoutPage.ClickPayByBankWire();
		checkoutPage.ClickConfirmOrder();
		//ASSERT
		assertEquals("Your order on My Store is complete.", checkoutPage.ReturnConfirmationText());
	}
	
	@Test
	@Order(3)
	public void GIVEN_a_test_site_WHEN_the_user_attempts_to_search_THEN_the_search_bar_functions_correctly() {
		//ARRANGE
		//Initialize pages
		LandingPage landingPage = new LandingPage(driver);
		//open browser with desired URL
		driver.get(url);
		
		//ACT
		//Input value into search 
		landingPage.InputSearchValue("Printed Dress");
		List<String> results = landingPage.ReturnSearchResults();
		
		//ASSERT
		for (String result: results) {
			assertThat(result, containsString("Printed"));
			assertThat(result, containsString("Dress"));
		}
	}
	
	@AfterEach
	public void TearDown() {
		driver.quit();
	}
}
