package resources.java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

public class BasePage {
	WebDriver _driver;
	protected BasePage(WebDriver driver){
		_driver = driver;
	}

	public void WaitForElementToBeVisible(){
		WebDriverWait wait = new WebDriverWait(_driver, 10);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.id("someid")));
	}
	
	public void ClickUsingJavascript(By selector) {
		WebElement element = _driver.findElement(selector);
		((JavascriptExecutor) _driver).executeScript("arguments[0].click();", element);
	}
}
