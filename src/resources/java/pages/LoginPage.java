package resources.java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.Random;

public class LoginPage extends BasePage {
	
	WebDriver _driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public LoginPage(WebDriver driver) {
		super(driver);
		_driver=driver;
	}
	
	//Locator for login button
	By EmailInput = By.id("email");
	By PasswordInput = By.id("passwd");
	By SignInButton = By.id("SubmitLogin");
	By RegisterEmailInput = By.id("email_create");
	By RegisterSubmitButton = By.id("SubmitCreate");
	
	//Method to click login button
	public void InputEmail(String email) {
		_driver.findElement(EmailInput).sendKeys(email);
	}
	
	public void InputPassword(String password) {
		_driver.findElement(PasswordInput).sendKeys(password);
	}
	
	public String InputRegisterEmail() {
		String userEmail = "test@test" + GenerateRandomNumber(10) + ".com";
		_driver.findElement(RegisterEmailInput).sendKeys(userEmail);
		return userEmail;
	}

	public void clickSignIn() {
		_driver.findElement(SignInButton).click();
	}

	public void clickCreateAnAccount() {
		_driver.findElement(RegisterSubmitButton).click();
	}
	
	private String GenerateRandomNumber(int numLength) {
        return String.valueOf(numLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, numLength - 1)) - 1)
                + (int) Math.pow(10, numLength - 1));
    }
}
