package resources.java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutPage extends BasePage {
	
	WebDriver _driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public CheckoutPage(WebDriver driver) {
		super(driver);
		_driver=driver;
	}

	//Locator for header items
	By TermsOfServiceCheckbox = By.id("cgv");
	By ProceedToCheckoutButton = By.xpath("//*/p[contains(@class, 'cart_navigation')]//span[contains(text(), 'Proceed')]");
	By PayByBankWireButton = By.xpath("//*/a[contains(@class, 'bankwire')]");
	By ConfirmOrderButton = By.xpath("//*/p/button");
	By OrderConfirmationText = By.xpath("//*/p[contains(@class, 'cheque-indent')]/strong");
	
	public void ClickProceedToCheckout() {
		ClickUsingJavascript(ProceedToCheckoutButton);
	}
	
	public void AcceptTermsOfService() {
		_driver.findElement(TermsOfServiceCheckbox).click();
	}
	
	public void ClickPayByBankWire() {
		_driver.findElement(PayByBankWireButton).click();
	}
	
	public void ClickConfirmOrder() {
		_driver.findElement(ConfirmOrderButton).click();
	}
	
	public String ReturnConfirmationText() {
		return _driver.findElement(OrderConfirmationText).getText();
	}

}

