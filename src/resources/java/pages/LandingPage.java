package resources.java.pages;

import java.util.List;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage extends BasePage {
	
	WebDriver _driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public LandingPage(WebDriver driver) {
		super(driver);
		_driver=driver;
	}

	//Locator for header items
	By LoginBtn = By.xpath("//*[@id='header']//a[contains(@class, 'login')]");
	By HeaderUserInfo = By.xpath("//*/div[@class='header_user_info']/a/span");
	
	//Locators for add to card
	By ProductButton = By.xpath("(//*/li[contains(@class, 'product')])[1]");
	By AddToCartButtons = By.xpath("//*[@id='add_to_cart']/button");
	By ProceedToCheckoutButton = By.xpath("//*/a[contains(@title, 'Proceed')]");
	
	//Search Locators
	By SearchInput = By.xpath("//input[@placeholder='Search']");
	By SearchResultsList = By.xpath("//*/div[contains(@class, 'results')]/ul/li");
	
	//Method to click login button
	public void clickLogin() {
		_driver.findElement(LoginBtn).click();
	}
	
	public String ReturnUserInfo() {
		return _driver.findElement(HeaderUserInfo).getText();
	}
	
	public void ClickProduct(int productNumber) {
		_driver.findElement(ProductButton).click();
	}
	
	public void AddToCart(int itemIndex) {
		_driver.findElement(AddToCartButtons).click();
	}
	
	public void ClickProceedToCheckout() {
		_driver.findElement(ProceedToCheckoutButton).click();
	}
	
	public void InputSearchValue(String value) {
		_driver.findElement(SearchInput).sendKeys(value);
	}
	
	public List<String> ReturnSearchResults() {
		List<String> results = new ArrayList<String>();
		List<WebElement> elements = _driver.findElements(SearchResultsList);
		for (WebElement element: elements) {
			results.add(element.getAttribute("innerText"));
		}
		return results;
	}

}
