## How to run the tests locally
1. Ensure you have JDK installed - https://www.oracle.com/java/technologies/javase-jdk16-downloads.html
2. Ensure you have maven installed - https://maven.apache.org/install.html
3. Copy or clone the repository
4. Command line to the project location (Directory containing src folder)
5. Run mvn test

## How to run the tests in a repo
1. Ensure you have a docker image available for use, set it to use JDK.16 - https://hub.docker.com/r/markhobson/maven-chrome/
2. Upload the code base to your docker reports
3. Ensure your yaml includes the following

<box>

image: markhobson/maven-chrome:jdk-16

stages:
  - build
  - test

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

cache:
  paths:
    - .m2/repository/
    - target/

build:
  stage: build
  script:
    - mvn compile

test:
  stage: test
  script:
    - mvn test
  artifacts:
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml

</box>

4. Ensure the POM file includes the following

<box>

    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>spriteCloud.Assessment</groupId>
    <artifactId>spriteCloud.Assessment</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>test</name>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.0.0-M3</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.1</version>
                    <configuration>
                        <source>16</source>
                        <target>16</target>
                    </configuration>
                </plugin>
            </plugins>
        </build>
    <dependencies>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-java</artifactId>
                <version>3.141.59</version>
            </dependency>
        <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-engine</artifactId>
                <version>5.5.2</version>
            </dependency>
                    <dependency>
                <groupId>io.github.bonigarcia</groupId>
                <artifactId>webdrivermanager</artifactId>
                <version>4.4.3</version>
            </dependency>
            <dependency>
                <groupId>io.rest-assured</groupId>
                <artifactId>rest-assured</artifactId>
                <version>4.3.0</version>
            </dependency>
    </dependencies>
    </project>
</box>
## Results
Results in Repo - https://gitlab.com/colonalk/assessment/-/tree/main/target/surefire-reports
Calliope Pro Results - https://app.calliope.pro/profiles/3576/reports

## What you used to select the scenarios, what was your approach?
I tried to get as much coverage as possible, across 3 different tests.

Initially I wanted to do something more pipeline oriented, where subsequent tests would fail, ie, login fails so dont even bother testing checkout. However since it is/was unlikely that the practice site would go down, I instead decided that I wanted to increase coverage. I had I gone with my initial approach, my test cases probably would have been, 1. Site Launches, 2. User can register, 3. User can login

## What could be the next steps on your project
Steps to improve the project.
* Implement a better file structure (switching halfway to Maven gave me a lot of unneeded issues, project should probably be created from scratch again to implement it)
* Move resources to a seperate project
* Implement a better driver handler, one that can perform cross-browser tests
* Implement proper class structure for headers for API tests (as well as a lot of general cleanup for this, which is my current goal)
* Move and create more methods for the helper class/create new helper classes to be used across tests.
